Source: ruby-jar-dependencies
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Miguel Landaeta <nomadium@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-jar-dependencies.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-jar-dependencies
Homepage: https://github.com/mkristian/jar-dependencies
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-jar-dependencies
Architecture: all
Depends: ruby:any,
         ${misc:Depends},
         ${shlibs:Depends}
Description: manage jar dependencies for Ruby gems
 jar-dependencies gem provides a simple way to load jars into JRuby's
 classloader. It also has additional and very convenient features like:
 allowing to declare jar dependencies in the gemspec files, vendoring
 jars with gems if needed, reuse jar files from Maven local
 repositories, lock down specific jar versions to avoid conflicts and
 support for proxy and mirrors functionalities thanks to a close
 integration with Maven.
 .
 The idea behind jar-dependencies is to do jar dependencies management
 in a correct way. It's somehow similar to the bundler tool but for
 jars.
